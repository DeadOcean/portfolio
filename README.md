# DEPRECATED

This repository will no longer be active, development will continue in a brand-new
monorepo powered by Turborepo, which can be found [here](https://gitlab.com/DeadOcean/ivanpartida.xyz)

# My Portfolio

Self-explanatory. Built with [Astro](https://astro.build/) and [TailwindCSS](https://tailwindcss.com/).

To do:

- Add a blog
- Add some easter eggs
